﻿using HsMetaTracker.Models;
using HsMetaTracker.Requests;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HsMetaTracker
{
    public static class Tournaments
    {
        public static void Show()
        {
            List<DeckMatchup> listOfMatchups = MetaStats.GetMatchup();
            Dictionary<string, double> metaDictionary = MetaStats.GetMetaForRank("all");
            var sortedMetaDictionary = metaDictionary.OrderByDescending(x => x.Value);
            Dictionary<string, string> archetypeDictionary = new Dictionary<string, string>();
            foreach (var archetype in sortedMetaDictionary)
            {
                if (archetypeDictionary.Count == 4)
                {
                    break;
                }

                var hero = listOfMatchups.FirstOrDefault(z => z.PlayerArchetype == archetype.Key)?.PlayerClass;

                if (hero != null &&
                    !archetypeDictionary.ContainsValue(hero))
                {
                    archetypeDictionary.Add(archetype.Key, hero);
                }
            }

            List<string> archetypeList = listOfMatchups.Select(x => x.PlayerArchetype).Distinct().ToList();
            Dictionary<string, double> matchups1 = new Dictionary<string, double>();
            Dictionary<string, double> matchups2 = new Dictionary<string, double>();
            Dictionary<string, double> matchups3 = new Dictionary<string, double>();
            Dictionary<string, double> matchups4 = new Dictionary<string, double>();
            List<string> tourArchetypeList = archetypeDictionary.Keys.ToList();

            foreach (var archetype in archetypeList)
            {
                double winRate1 = 0;
                double winRate2 = 0;
                double winRate3 = 0;
                double winRate4 = 0;
                var matchupList = listOfMatchups.Where(x => x.PlayerArchetype == archetype);

                foreach (var matchup in matchupList)
                {
                    if (tourArchetypeList[0] == matchup.OpponentArchetype)
                    {
                        winRate2 = winRate2 + matchup.WinRate / 100 * 33;
                        winRate3 = winRate3 + matchup.WinRate / 100 * 33;
                        winRate4 = winRate4 + matchup.WinRate / 100 * 33;
                    }

                    if (tourArchetypeList[1] == matchup.OpponentArchetype)
                    {
                        winRate1 = winRate1 + matchup.WinRate / 100 * 33;
                        winRate3 = winRate3 + matchup.WinRate / 100 * 33;
                        winRate4 = winRate4 + matchup.WinRate / 100 * 33;
                    }

                    if (tourArchetypeList[2] == matchup.OpponentArchetype)
                    {
                        winRate1 = winRate1 + matchup.WinRate / 100 * 33;
                        winRate2 = winRate2 + matchup.WinRate / 100 * 33;
                        winRate4 = winRate4 + matchup.WinRate / 100 * 33;
                    }

                    if (tourArchetypeList[3] == matchup.OpponentArchetype)
                    {
                        winRate1 = winRate1 + matchup.WinRate / 100 * 33;
                        winRate2 = winRate2 + matchup.WinRate / 100 * 33;
                        winRate3 = winRate3 + matchup.WinRate / 100 * 33;
                    }
                }

                matchups1.Add(archetype, winRate1);
                matchups2.Add(archetype, winRate2);
                matchups3.Add(archetype, winRate3);
                matchups4.Add(archetype, winRate4);
            }

            var sorted1 = matchups1.OrderByDescending(x => x.Value);
            var sorted2 = matchups2.OrderByDescending(x => x.Value);
            var sorted3 = matchups3.OrderByDescending(x => x.Value);
            var sorted4 = matchups4.OrderByDescending(x => x.Value);

            File.WriteAllText("D:\\tour.txt", BuildExport(tourArchetypeList, sorted1, sorted2, sorted3, sorted4));

        }

        private static string BuildExport(List<string> tourArchetypeList, IEnumerable<KeyValuePair<string, double>> matchups1, IEnumerable<KeyValuePair<string, double>> matchups2,
            IEnumerable<KeyValuePair<string, double>> matchups3, IEnumerable<KeyValuePair<string, double>> matchups4)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(tourArchetypeList[1] + ", " + tourArchetypeList[2] + ", " + tourArchetypeList[3]);
            foreach (var arch in matchups1)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }
            sb.AppendLine("");

            sb.AppendLine(tourArchetypeList[0] + ", " + tourArchetypeList[2] + ", " + tourArchetypeList[3]);
            foreach (var arch in matchups2)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }
            sb.AppendLine("");

            sb.AppendLine(tourArchetypeList[0] + ", " + tourArchetypeList[1] + ", " + tourArchetypeList[3]);
            foreach (var arch in matchups3)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }
            sb.AppendLine("");

            sb.AppendLine(tourArchetypeList[0] + ", " + tourArchetypeList[1] + ", " + tourArchetypeList[2]);
            foreach (var arch in matchups4)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }
            sb.AppendLine("");

            return sb.ToString();
        }
    }
}

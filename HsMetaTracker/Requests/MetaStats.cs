﻿using HsMetaTracker.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace HsMetaTracker.Requests
{
    public static class MetaStats
    {
        private const string matchupCsvLink = "http://metastats.net/archetype/matchup/download";
        private const string metaBaseLink = "http://metastats.net/archetype.php?range=today&region=eu&rank=";

        public static List<DeckMatchup> GetMatchup()
        {
            int lineNumber = 0;
            List<DeckMatchup> splitted = new List<DeckMatchup>();
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(matchupCsvLink);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
            {
                while (sr.Peek() >= 0)
                {
                    lineNumber++;
                    Debug.Print(lineNumber.ToString());
                    string result = sr.ReadLine();
                    if (lineNumber > 1)
                    {
                        DeckMatchup matchupModel = GetMatchupModel(result);
                        if (matchupModel != null)
                        {
                            splitted.Add(matchupModel);
                        }
                    }
                }
            }

            return splitted;
        }

        public static Dictionary<string, double> GetMetaForRank(string rank)
        {
            Dictionary<string, double> metaDictionary = new Dictionary<string, double>();
            string metaLink = metaBaseLink + rank;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(metaLink);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            JObject json = JObject.Parse(sr.ReadToEnd());
            JToken rows = json["rows"];
            foreach (var row in rows)
            {
                var metaClass = row.First.First;
                metaDictionary.Add((string)metaClass[0].First.First, (double)metaClass[1].First.First);
            }
            sr.Close();
            return metaDictionary;
        }

        private static DeckMatchup GetMatchupModel(string line)
        {
            var parameters = line.Replace("\"", "").Split(',');
            if (parameters.Length >= 6)
            {
                DeckMatchup dm = new DeckMatchup();
                dm.PlayerClass = parameters[0];
                dm.PlayerArchetype = parameters[1].Replace("\\", "");
                dm.OpponentClass = parameters[2];
                dm.OpponentArchetype = parameters[3];
                dm.WinRate = double.Parse(parameters[4], NumberStyles.Any, CultureInfo.InvariantCulture);
                dm.TotalGames = Convert.ToInt32(parameters[5]);

                return dm;
            }
            else
            {
                return null;
            }
        }
    }
}

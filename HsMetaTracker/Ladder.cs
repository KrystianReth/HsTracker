﻿using HsMetaTracker.Models;
using HsMetaTracker.Requests;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace HsMetaTracker
{
    public static class Ladder
    {
        public static void Show()
        {
            List<DeckMatchup> listOfMatchups = MetaStats.GetMatchup();
            Dictionary<string, double> metaDictionary5 = MetaStats.GetMetaForRank("5");
            Dictionary<string, double> metaDictionary10 = MetaStats.GetMetaForRank("10");
            Dictionary<string, double> metaDictionary20 = MetaStats.GetMetaForRank("20");
            double metaPercent5 = metaDictionary5.Sum(x => x.Value);
            double metaPercent10 = metaDictionary10.Sum(x => x.Value);
            double metaPercent20 = metaDictionary20.Sum(x => x.Value);
            List<string> archetypeList = listOfMatchups.Select(x => x.PlayerArchetype).Distinct().ToList();
            Dictionary<string, double> matchupsOnActualMeta5 = new Dictionary<string, double>();
            Dictionary<string, double> matchupsOnActualMeta10 = new Dictionary<string, double>();
            Dictionary<string, double> matchupsOnActualMeta20 = new Dictionary<string, double>();

            foreach (var archetype in archetypeList)
            {
                double winRate5 = 0;
                double winRate10 = 0;
                double winRate20 = 0;

                var matchupList = listOfMatchups.Where(x => x.PlayerArchetype == archetype);
                foreach (var matchup in matchupList)
                {
                    if (metaDictionary5.ContainsKey(matchup.OpponentArchetype))
                    {
                        winRate5 = winRate5 + matchup.WinRate / metaPercent5 * metaDictionary5[matchup.OpponentArchetype];
                    }

                    if (metaDictionary10.ContainsKey(matchup.OpponentArchetype))
                    {
                        winRate10 = winRate10 + matchup.WinRate / metaPercent10 * metaDictionary10[matchup.OpponentArchetype];
                    }

                    if (metaDictionary20.ContainsKey(matchup.OpponentArchetype))
                    {
                        winRate20 = winRate20 + matchup.WinRate / metaPercent20 * metaDictionary20[matchup.OpponentArchetype];
                    }
                }

                matchupsOnActualMeta5.Add(archetype, winRate5);
                matchupsOnActualMeta10.Add(archetype, winRate10);
                matchupsOnActualMeta20.Add(archetype, winRate20);
            }

            var sorted5 = matchupsOnActualMeta5.OrderByDescending(x => x.Value);
            var sorted10 = matchupsOnActualMeta10.OrderByDescending(x => x.Value);
            var sorted20 = matchupsOnActualMeta20.OrderByDescending(x => x.Value);

            File.WriteAllText("D:\\ladder.txt", BuildExport(sorted5, sorted10, sorted20));
        }

        private static string BuildExport(IEnumerable<KeyValuePair<string, double>> sorted5, 
            IEnumerable<KeyValuePair<string, double>> sorted10, 
            IEnumerable<KeyValuePair<string, double>> sorted20)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Legent to 5");
            foreach (var arch in sorted5)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }
            sb.AppendLine("");

            sb.AppendLine("5 to 10");
            foreach (var arch in sorted10)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }
            sb.AppendLine("");

            sb.AppendLine("10 to 20");
            foreach (var arch in sorted20)
            {
                sb.AppendLine(arch.Key + " " + arch.Value);
            }

            return sb.ToString();
        }
    }
}

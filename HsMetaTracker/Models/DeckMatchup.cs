﻿namespace HsMetaTracker.Models
{
    public class DeckMatchup
    {
        public string PlayerClass { get; set; }
        public string OpponentClass { get; set; }
        public string PlayerArchetype { get; set; }
        public string OpponentArchetype { get; set; }
        public double WinRate { get; set; }
        public int TotalGames { get; set; }
    }
}
